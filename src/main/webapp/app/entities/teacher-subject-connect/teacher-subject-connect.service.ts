import { Injectable, isDevMode } from '@angular/core';
import { ApplicationConfigService } from '../../core/config/application-config.service';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { ITeacherSubject } from '../teacher-subject/teacher-subject.model';

type EntityArrayResponseType = HttpResponse<ITeacherSubject[]>;

@Injectable({
  providedIn: 'root',
})
export class TeacherSubjectConnectService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/teacher-subject-connect');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  subjectConnect(id: number | undefined): Observable<EntityArrayResponseType> {
    return isDevMode()
      ? of(
          new HttpResponse({
            body: [
              {
                id: 1,
                teacher: { id: 1, fullName: 'Full name teacher' },
                subject: { id: 2, name: 'Name subject' },
              },
              {
                id: 2,
                teacher: { id: 2, fullName: 'Full name teacher 2' },
                subject: { id: 3, name: 'Name subject 3' },
              },
            ],
          })
        )
      : this.http.get<ITeacherSubject[]>(`${this.resourceUrl}/by-teacher/${id}`, { observe: 'response' });
  }

  create(data: { teacherId: number | undefined; subjects: any }) {
    return this.http.post(this.resourceUrl, data, { observe: 'response' });
  }
}
