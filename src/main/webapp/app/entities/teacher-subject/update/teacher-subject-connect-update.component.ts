import { Component, OnInit } from '@angular/core';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { SubjectService } from '../../subject/service/subject.service';
import { ISubject } from '../../subject/subject.model';
import { NgForOf } from '@angular/common';
import { NzCheckboxModule } from 'ng-zorro-antd/checkbox';
import { FormsModule } from '@angular/forms';
import { NzTableModule } from 'ng-zorro-antd/table';
import { TeacherSubjectConnectService } from '../../teacher-subject-connect/teacher-subject-connect.service';
import { NzModalService } from 'ng-zorro-antd/modal';
import { NzMessageService } from 'ng-zorro-antd/message';
import SharedModule from '../../../shared/shared.module';

@Component({
  selector: 'jhi-teacher-subject-connect-update',
  templateUrl: './teacher-subject-connect-update.component.html',
  imports: [SharedModule, NzButtonModule, NgForOf, NzCheckboxModule, FormsModule, NzTableModule],
  standalone: true,
  providers: [NzMessageService],
})
export class TeacherSubjectConnectUpdateComponent implements OnInit {
  subjects: ISubject[] = [];
  checked = false;
  setOfCheckedId = new Set<number>();
  indeterminate = false;
  totalHours = 0;
  teacherId?: number;

  constructor(
    private readonly subjectService: SubjectService,
    private readonly teacherSubjectConnectService: TeacherSubjectConnectService,
    protected readonly nzModal: NzModalService,
    private readonly nzMessage: NzMessageService
  ) {}

  ngOnInit() {
    this.subjectService.query().subscribe(data => {
      if (data.body) this.subjects = data.body;
    });
  }

  updateCheckedSet(id: number, checked: boolean): void {
    if (checked) {
      this.setOfCheckedId.add(id);
    } else {
      this.setOfCheckedId.delete(id);
    }
    this.totalHours = 0;
    this.subjects.forEach(subject => {
      if (this.setOfCheckedId.has(subject.id) && subject.hours) {
        this.totalHours += subject.hours;
      }
    });
  }

  refreshCheckedStatus(): void {
    const listOfEnabledData = this.subjects;
    this.checked = listOfEnabledData.every(({ id }) => this.setOfCheckedId.has(id));
    this.indeterminate = listOfEnabledData.some(({ id }) => this.setOfCheckedId.has(id)) && !this.checked;
  }

  onAllChecked(checked: boolean): void {
    this.subjects.forEach(({ id }) => this.updateCheckedSet(id, checked));
    this.refreshCheckedStatus();
  }

  onItemChecked(id: number, checked: boolean): void {
    this.updateCheckedSet(id, checked);
    this.refreshCheckedStatus();
  }

  save() {
    this.teacherSubjectConnectService.create({ teacherId: this.teacherId, subjects: [...this.setOfCheckedId] }).subscribe(data => {
      this.nzModal.closeAll();
      this.nzMessage.success('Success create!');
    });
  }
}
