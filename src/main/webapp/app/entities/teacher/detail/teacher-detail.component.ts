import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, RouterModule } from '@angular/router';

import SharedModule from 'app/shared/shared.module';
import { DurationPipe, FormatMediumDatetimePipe, FormatMediumDatePipe } from 'app/shared/date';
import { ITeacher } from '../teacher.model';
import { AlertErrorComponent } from '../../../shared/alert/alert-error.component';
import { AlertComponent } from '../../../shared/alert/alert.component';
import TranslateDirective from '../../../shared/language/translate.directive';
import { NgForOf, NgIf } from '@angular/common';
import { ITeacherSubject } from '../../teacher-subject/teacher-subject.model';
import { NzModalService } from 'ng-zorro-antd/modal';
import { TeacherSubjectConnectUpdateComponent } from '../../teacher-subject/update/teacher-subject-connect-update.component';
import { TeacherSubjectConnectService } from '../../teacher-subject-connect/teacher-subject-connect.service';

@Component({
  standalone: true,
  selector: 'jhi-teacher-detail',
  templateUrl: './teacher-detail.component.html',
  imports: [
    SharedModule,
    RouterModule,
    DurationPipe,
    FormatMediumDatetimePipe,
    FormatMediumDatePipe,
    AlertErrorComponent,
    AlertComponent,
    TranslateDirective,
    NgIf,
    TranslateDirective,
    SharedModule,
    NgForOf,
  ],
  providers: [NzModalService],
})
export class TeacherDetailComponent implements OnInit {
  @Input() teacher: ITeacher | null = null;
  teacherSubjectConnects?: ITeacherSubject[];

  constructor(
    protected activatedRoute: ActivatedRoute,
    private readonly teacherSubjectConnectService: TeacherSubjectConnectService,
    private readonly nzModalService: NzModalService
  ) {}

  ngOnInit() {
    this.teacherSubjectConnectService.subjectConnect(this.teacher?.id).subscribe(data => {
      if (data.body) this.teacherSubjectConnects = data.body;
    });
  }

  previousState(): void {
    window.history.back();
  }

  addConnect() {
    this.nzModalService.create({
      nzContent: TeacherSubjectConnectUpdateComponent,
      nzFooter: null,
      nzTitle: '',
      nzWidth: '680px',
      nzComponentParams: {
        teacherId: this.teacher?.id,
      },
    });
  }
}
