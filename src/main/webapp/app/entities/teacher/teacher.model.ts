import { IStaff } from 'app/entities/staff/staff.model';
import { ISpec } from 'app/entities/spec/spec.model';
import { AcademicRank } from 'app/entities/enumerations/academic-rank.model';

export interface ITeacher {
  id: number;
  fullName?: string | null;
  rank?: keyof typeof AcademicRank | null;
  staff?: IStaff | null;
  spec?: ISpec | null;
}

export type NewTeacher = Omit<ITeacher, 'id'> & { id: null };
